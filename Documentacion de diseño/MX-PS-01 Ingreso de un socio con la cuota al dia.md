# Objetivos asociados

[MX-OB-01: Controlar el acceso de un socio en función del pago de las cuotas.](MX-OB-01 Controlar el acceso de un socio en función del pago de las cuotas.md)

[MX-OB-02: Registrar cada ingreso de los socios y empleados.](MX-OB-02 Registrar cada ingreso de los socios y empleados.md)

[MX-OB-03: Evitar el fraude al cliente por parte de los empleados y/o socios.](MX-OB-03 Evitar el fraude al cliente por parte de los empleados yo socios.md)

[MX-OB-04: Gestionar la información de los socios.](MX-OB-04 Gestionar la información de los socios.md)

# Requisitos asociados

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

[MX-RF-02: Mantener registro de los pagos de los socios.](MX-RF-02 Mantener registro de los pagos de los socios.md)

[MX-RF-03: Mantener un registro eventos de ingreso.](MX-RF-03 Mantener un registro eventos de ingreso.md)

[MX-RF-04: Identificar a un socio mediante un lector de huellas digitales.](MX-RF-04 Identificar a un socio mediante un lector de huellas digitales.md)

[MX-RF-05: Permitir el ingreso de los socios con las cuotas pagas.](MX-RF-05 Permitir el ingreso de los socios con las cuotas pagas.md)

# Pasos

1. 
   * **Acción:** Crear un socio ingresando Nombre, Apellido y DNI. 
   * **Resultado: **Se pueden registrar los datos del socio en el sistema.
2. 
   * **Acción**: Enrolar una huella del nuevo socio.
   * **Resultado**: Se asocia una huella al socio en el sistema.
3. 
   * **Acción:** Registrar el pago por un mes.
   * **Resultado:** Se registra correctamente el pago de la cuota.
4. 
   * **Acción:** EL socio intenta ingresar utilizando el dedo enrolado.
   * **Resultado:** El sistema autoriza el acceso del socio.
5. 
6. * **Acción:** El socio ingresa a las instalaciones
   * **Resultado:** Se registra el ingreso en el historial de movimientos.