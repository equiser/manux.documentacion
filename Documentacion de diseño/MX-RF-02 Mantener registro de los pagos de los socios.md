# Casos de uso relacionados

[MX-CU-02 Cobro de cuota a un socio](MX-CU-02 Cobro de cuota a un socio.md)

# Pruebas relacionadas

[MX-PS-01: Ingreso de un socio con la cuota al dia.](MX-PS-01 Ingreso de un socio con la cuota al dia.md)

[MX-PF-05: Cobro de la cuota a un socio.](MX-PF-05 Cobro de la cuota a un socio.md)