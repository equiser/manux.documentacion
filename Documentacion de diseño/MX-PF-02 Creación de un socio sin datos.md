# Requisitos asociados

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

# Pasos

1. 
   * **Acción**: Seleccionar la función nuevo socio.
   * **Resultado:** Se despliega la ficha con los datos de un socio en blanco.
2. 
   * **Acción:** Confirmar los datos para la creación del socio.
   * **Resultado:** Los datos no son aceptados.
   * Resultado: Se informa que el campo nombre es obligatorio.
   * Resultado: Se informa que el campo apellido es obligatorio.
   * Resultado: Se informa que el campo documento es obligatorio