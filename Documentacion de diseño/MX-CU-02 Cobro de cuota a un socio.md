# Objetivos asociados

[MX-OB-01: Controlar el acceso de un socio en función del pago de las cuotas.](MX-OB-01 Controlar el acceso de un socio en función del pago de las cuotas.md)

[MX-OB-03: Evitar el fraude al cliente por parte de los empleados y/o socios.](MX-OB-03 Evitar el fraude al cliente por parte de los empleados yo socios.md)

# Requisitos asociados

[MX-RF-02: Mantener registro de los pagos de los socios.](MX-RF-02 Mantener registro de los pagos de los socios.md)

[MX-RF-05: Permitir el ingreso de los socios con las cuotas pagas.](MX-RF-05 Permitir el ingreso de los socios con las cuotas pagas.md)

[MX-RF-06: Impedir el ingreso de los socios con las cuotas atrasadas.](MX-RF-06 Impedir el ingreso de los socios con las cuotas atrasadas.md)

[MX-NF-01: El sistema estará desarrollado utilizando tecnología web.](MX-NF-01 El sistema estará desarrollado utilizando tecnología web.md)

# Actores

* Empleado
* Socio

# Precondiciones

* El empleado debe haber ingresado al sistema como usuario
* El socio debe haber sido dado de alta en el sistema ([MX-CU-01 Actualización de un socio](MX-CU-01 Actualización de un socio.md)).

# Secuencia normal

1. El empleado solicita registrar el pago de una cuota.
2. El empleado busca en la lista de socios mediante el Apellido o el DNI. 
3. El empleado especifica el valor abonado y el periodo correspondiente.
4. El sistema valida la información ingresada.
5. El sistema registra el pago y extiende la fecha de vencimiento.

# Secuencias alternativas

* 2a. El socio no se encuentra en la lista.
  * 2a1. El sistema muestra un mensaje error.
* 4a. Los datos ingresados son incosistentes.
  * 4a1. El sistema muestra un mensaje de error.

# Postcondiciones

* El socio está autorizado a ingresar a las instalaciones hasta la fecha de vencimiento.