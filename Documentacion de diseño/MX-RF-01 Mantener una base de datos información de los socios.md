# Casos de uso relacionados

[MX-CU-01 Actualización de un socio](MX-CU-01 Actualización de un socio.md)

# Pruebas relacionadas

[MX-PS-01: Ingreso de un socio con la cuota al dia.](MX-PS-01 Ingreso de un socio con la cuota al dia.md)

[MX-PS-02: Rechazo de un socio con la cuota vencida.](MX-PS-02 Rechazo de un socio con la cuota vencida.md)

[MX-PF-01: Creación de un socio con datos correctos.](MX-PF-01 Creación de un socio con datos correctos.md)

[MX-PF-02: Creación de un socio sin datos.](MX-PF-02 Creación de un socio sin datos.md)

[MX-PF-03: Creación de un socio con documento duplicado.](MX-PF-03 Creación de un socio con documento duplicado.md)