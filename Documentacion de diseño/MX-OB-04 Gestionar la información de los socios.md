# Casos de uso relacionados

[MX-CU-01 Actualización de un socio](MX-CU-01 Actualización de un socio.md)

# Pruebas relacionadas

[MX-PS-01: Ingreso de un socio con la cuota al dia.](MX-PS-01 Ingreso de un socio con la cuota al dia.md)

[MX-PS-02: Rechazo de un socio con la cuota vencida.](MX-PS-02 Rechazo de un socio con la cuota vencida.md)