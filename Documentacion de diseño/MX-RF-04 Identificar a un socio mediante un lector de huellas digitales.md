# Casos de uso relacionados

[MX-CU-03 Ingreso de un socio](MX-CU-03 Ingreso de un socio.md)

# Pruebas relacionadas

[MX-PS-01: Ingreso de un socio con la cuota al dia.](MX-PS-01 Ingreso de un socio con la cuota al dia.md)

[MX-PS-02: Rechazo de un socio con la cuota vencida.](MX-PS-02 Rechazo de un socio con la cuota vencida.md)

[MX-PF-04: Registro de la huella de un socio.](MX-PF-04 Registro de la huella de un socio.md)