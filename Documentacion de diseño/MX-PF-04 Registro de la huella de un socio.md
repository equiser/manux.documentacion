# Requisitos asociados

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

[MX-RF-04: Identificar a un socio mediante un lector de huellas digitales.](MX-RF-04 Identificar a un socio mediante un lector de huellas digitales.md)

# Pasos

1. .
   * **Acción**: Seleccionar la función nuevo socio.
   * **Resultado:** Se despliega la ficha con los datos de un socio en blanco.
2. .
   * **Acción:** Ingresar los siguientes datos:
     * Nombre: **Nicolas Alejandro**
     * Apellido: **VOLENTINI VILLAFAÑE**
     * Documento: **43163059**


   * **Resultado:** Se pueden ingresar todos los datos.
3. .
   * **Acción:** Confirmar los datos para la creación del socio.
   * **Resultado:** Los datos son aceptados y se crea el nuevo socio.
4. .
   * **Acción:** Seleccionar la función de enrolar una nueva huella.
   * **Resultado:** Se despliega la ficha con la selección del dedo a registrar.
5. .
   * **Acción:** Se selecciona el dedo indice de la mano derecha
   * **Resultado:** Se acepta la selección y se informa que se debe colocar el dedo sobre el lector una primera vez de un total de tres veces.
6. .
   * **Acción:** El socio coloca el dedo indice derecho en el lector biométrico.
   * **Resultado:** Se confirma la lectura correcta y se pide repetir el proceso una segunda vez.
7. .
   * **Acción:** El socio coloca el dedo indice derecho en el lector biométrico.
   * **Resultado:** Se confirma la lectura correcta y se pide repetir el proceso una tercera vez.
8. .
   * **Acción:** El socio coloca el dedo indice derecho en el lector biométrico.
   * **Resultado:** Se confirma el final del proceso exitoso.
   * **Resultado:** Se almacena la huella en la base da datos.