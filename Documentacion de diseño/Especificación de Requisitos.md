# Introducción

El presente documento describe la Especificación de Requisitos del Sistema (ERS) con el fin de proporcionar la información necesaria para el correcto entendimiento de los requerimientos que se deben satisfacer tanto para la dirección de la empresa como para los desarrolladores.

## Proposito

El presente documento pretende establecer las bases de acuerdo acerca de las características del sistema y ayudar a la dirección de la empresa a entender exactamente el producto que se desea desarrollar.

## Ambito del Sistema

El sistema planteado tiene por objetivo principal el control del ingreso de los socios de una entidad que deben abonar una cuota periódica para el uso de las instalaciones, utilizando una característica biométrica para identificar al mismo.

## Definiciones

**Cliente:** Persona o empresa dueña o responsable de la institución (club, gimnasio, etc) que adquiere el sistema para el control de los socios y empleados.

**Empleado:** Persona designada por el cliente para atender y controlar a los socios.

**Socio:** Persona que abona una cuota para el uso de las instalaciones del cliente. 

## Codificación

**MX-CU-XX:** Caso de uso.

**MX-OB-XX:** Objetivo inmediato.

**MX-OP-XX:** Objetivo futuro.

**MX-RF-XX:** Requisito funcional.

**MX-NF-XX:** Requisito no funcional.

**MX-PS-XX:** Prueba de sistema.

**MX-PI-XX:** Prueba de integración

**MX-PU-XX:** Prueba unitaria.

## 

## Casos de uso

![Casos de uso.png](CasosUso.png)

## Referencias

[MX-CU-01 Actualización de un socio](MX-CU-01 Actualización de un socio.md)

[MX-CU-02 Cobro de cuota a un socio](MX-CU-02 Cobro de cuota a un socio.md)

[MX-CU-03 Ingreso de un socio](MX-CU-03 Ingreso de un socio.md)

# Descripción General

## Perspectiva del producto

El sistema a desarrollar es independiente de cualquier otro sistema existente. Se busca ademas que el sistema sea lo más independiente posible del equipamiento informático existente en el cliente, minimizando de esta forma los problemas de compatibilidad con el hardware y las versiones de sistemas operativos utilizadas en las computadoras del cliente.

Para el desarrollo se basa en molinete que incluye una placa con la capacidad de ejecutar un sistema basado en Linux donde ejecute un servidor WEB que permitirá toda la gestión del sistema y ademas un proceso de tiempo real que supervisará un lector de huellas digitales, una pantalla alfanumérica y el control de molinete para permitir o impedir el acceso en función de los pagos de los socios. 

## Funcionalidad del producto

El producto deberá cumplir con los siguientes requisitos generales:

[MX-OB-01: Controlar el acceso de un socio en función del pago de las cuotas.](MX-OB-01 Controlar el acceso de un socio en función del pago de las cuotas.md)

[MX-OB-02: Registrar cada ingreso de los socios y empleados.](MX-OB-02 Registrar cada ingreso de los socios y empleados.md)

[MX-OB-03: Evitar el fraude al cliente por parte de los empleados y/o socios.](MX-OB-03 Evitar el fraude al cliente por parte de los empleados yo socios.md)

[MX-OF-01: Gestionar la información de los empleados.](MX-OF-01 Gestionar la información de los empleados.md)

[MX-OB-04: Gestionar la información de los socios.](MX-OB-04 Gestionar la información de los socios.md)

## Características de los usuarios

Los usuarios finales de este producto son los socios de la entidad cliente los cuales responden a una variedad muy amplia de perfiles de edad, educación, capacidades físicas y experiencia. 

Adicionalmente serán también usuarios del sistema los empleados del cliente, los cuales son personas mayores de edad, por lo general deportistas con un nivel educativo de secundario completo y experiencia básica en el uso de computadoras.

## Restricciones

* Todo el desarrollo debe mantenerse bajo control de versiones, tanto los documentos como el código y los desarrollos de equipos si existieran.
* El sistema será operado desde una computadora existente en el cliente. El sistema deberá tener el menor acoplamiento posible con esta maquina, permitiendo la mayor libertad posible en cuanto a la capacidad de procesamiento de la misma, como así ambienten del sistema operativo utilizado.
* El sistema deberá contemplar perfiles de usuarios a fin de restringir el acceso a funciones que pudieran derivar en un fraude por parte de los empleados hacia el cliente. 
* La función de control del acceso de los socios es critica en cuanto a tiempo de respuesta, confiabilidad del acceso y disponibilidad del sistema.

## Suposiciones y Dependencias

Se supone que el cliente cuenta con una red Ethernet a la cual se conectará el equipo que contiene el sistema al igual que la computadora desde la cual se realizará la gestión del mismo.

## Ampliaciones futuras

[MX-OF-02: Registrar venta de productos y servicios adicionales.](MX-OF-02 Registrar venta de productos y servicios adicionales.md)

[MX-OF-03: Controlar el stock de los producto que se venden.](MX-OF-03 Controlar el stock de los producto que se venden.md)

# Requisitos específicos

## Requisitos funcionales esenciales

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

[MX-RF-02: Mantener registro de los pagos de los socios.](MX-RF-02 Mantener registro de los pagos de los socios.md)

[MX-RF-03: Mantener un registro eventos de ingreso.](MX-RF-03 Mantener un registro eventos de ingreso.md)

[MX-RF-04: Identificar a un socio mediante un lector de huellas digitales.](MX-RF-04 Identificar a un socio mediante un lector de huellas digitales.md)

[MX-RF-05: Permitir el ingreso de los socios con las cuotas pagas.](MX-RF-05 Permitir el ingreso de los socios con las cuotas pagas.md)

[MX-RF-06: Impedir el ingreso de los socios con las cuotas atrasadas.](MX-RF-06 Impedir el ingreso de los socios con las cuotas atrasadas.md)

## Requisitos no funcionales

[MX-NF-01: El sistema estará desarrollado utilizando tecnología web.](MX-NF-01 El sistema estará desarrollado utilizando tecnología web.md)

[MX-NF-02: Se utilizará un lector de huellas autónomo con comunicación serial TTL.](MX-NF-02 Se utilizará un lector de huellas autónomo con comunicación serial TTL.md)