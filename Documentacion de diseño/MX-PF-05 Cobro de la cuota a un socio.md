# Requisitos asociados

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

[MX-RF-02: Mantener registro de los pagos de los socios.](MX-RF-02 Mantener registro de los pagos de los socios.md)

# Pasos

1. .
   * **Acción**: Seleccionar la función nuevo socio.
   * **Resultado:** Se despliega la ficha con los datos de un socio en blanco.
2. .
   * **Acción:** Ingresar los siguientes datos:
     * Nombre: **Nicolas Alejandro**
     * Apellido: **VOLENTINI VILLAFAÑE**
     * Documento: **43163059**


   * **Resultado:** Se pueden ingresar todos los datos.
3. .
   * **Acción:** Confirmar los datos para la creación del socio.
   * **Resultado:** Los datos son aceptados y se crea el nuevo socio.
4. .
   * **Acción:** Seleccionar la función registrar pago de cuota.
   * **Resultado:** Se despliega la ficha para registrar un nuevo pago.
5. .
   * **Acción:** Ingresar los siguientes datos:
     * Socio: El registrado en el paso 2.
     * Monto: **$500**
     * Periodo: **1 Mes**
   * **Resultado:** Se pueden ingresar todos los datos.
6. .
   * **Acción:** Confirmar los datos del pago de la cuota.
   * **Resultado:** Los datos son aceptados y se carga el pago.
   * **Resultado:** La fecha de vencimiento del ingreso se programa para el mismo día de la fecha actual pero del mes proximo.