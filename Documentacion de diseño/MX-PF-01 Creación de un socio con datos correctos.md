# Requisitos asociados

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

# Pasos

1. 
   * **Acción**: Seleccionar la función nuevo socio.
   * **Resultado:** Se despliega la ficha con los datos de un socio en blanco.
2. 
   * **Acción:** Ingresar los siguientes datos:
   1. * Nombre: **Nicolas Alejandro**
   2. * Apellido: **VOLENTINI VILLAFAÑE**
   3. * Documento: **43163059**
   * **Resultado:** Se pueden ingresar todos los datos.
3. 
   * **Acción:** Confirmar los datos para la creación del socio.
   * **Resultado:** Los datos son aceptados y se crea el nuevo socio.