# Objetivos asociados

[MX-OB-04: Gestionar la información de los socios.](MX-OB-04 Gestionar la información de los socios.md)

# Requisitos asociados

[MX-RF-01: Mantener una base de datos información de los socios.](MX-RF-01 Mantener una base de datos información de los socios.md)

[MX-NF-01: El sistema estará desarrollado utilizando tecnología web.](MX-NF-01 El sistema estará desarrollado utilizando tecnología web.md)

# Actores

* Empleado
* Socio

# Precondiciones

* El empleado debe haber ingresado al sistema como usuario

# Secuencia normal

1. El empleado solicita la creación de un nuevo socio.
2. El empleado ingresa obligatoriamente los siguientes datos: Nombre, Apellido y DNI. 
3. El sistema valida los datos ingresados del socio.
4. El empleado solicita el registro de una huella digital del socio.
5. El socio coloca el dedo designado en el lector de huella.
6. El sistema confirma el enrolamiento de la huella digital.

# Secuencias alternativas

* 3a. No se ingresa uno de los datos obligatorios.
  * 3a1. El sistema muestra un mensaje de error.
* 3b. El DNI ingresado ya existe en el sistema.
  * 3b1. El sistema muestra un mensaje de error. 
* 6a. El sistema no puede completar el enrolado de la huella
  * 6a1. El sistema muestra un mensaje de error.

# Postcondiciones

* El socio queda registrado en el sistema sin cuotas pagas.
* El socio no está autorizado a ingresar a las instalaciones.