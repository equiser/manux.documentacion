# Casos de uso relacionados

[MX-CU-02 Cobro de cuota a un socio](MX-CU-02 Cobro de cuota a un socio.md)

[MX-CU-03 Ingreso de un socio](MX-CU-03 Ingreso de un socio.md)

# Pruebas relacionadas

[MX-PS-02: Rechazo de un socio con la cuota vencida.](MX-PS-02 Rechazo de un socio con la cuota vencida.md)