# Objetivos asociados

[MX-OB-01: Controlar el acceso de un socio en función del pago de las cuotas.](MX-OB-01 Controlar el acceso de un socio en función del pago de las cuotas.md)

[MX-OB-02: Registrar cada ingreso de los socios y empleados.](MX-OB-02 Registrar cada ingreso de los socios y empleados.md)

[MX-OB-03: Evitar el fraude al cliente por parte de los empleados y/o socios.](MX-OB-03 Evitar el fraude al cliente por parte de los empleados yo socios.md)

# Requisitos asociados

[MX-RF-03: Mantener un registro eventos de ingreso.](MX-RF-03 Mantener un registro eventos de ingreso.md)

[MX-RF-04: Identificar a un socio mediante un lector de huellas digitales.](MX-RF-04 Identificar a un socio mediante un lector de huellas digitales.md)

[MX-RF-05: Permitir el ingreso de los socios con las cuotas pagas.](MX-RF-05 Permitir el ingreso de los socios con las cuotas pagas.md)

[MX-RF-06: Impedir el ingreso de los socios con las cuotas atrasadas.](MX-RF-06 Impedir el ingreso de los socios con las cuotas atrasadas.md)

[MX-NF-02: Se utilizará un lector de huellas autónomo con comunicación serial TTL.](MX-NF-02 Se utilizará un lector de huellas autónomo con comunicación serial TTL.md)

# Actores

* Socio

# Precondiciones

* El socio debe haber sido dado de alta en el sistema ([MX-CU-01 Actualización de un socio](MX-CU-01 Actualización de un socio.md)).

# Secuencia normal

1. El socio coloca uno de los dedos enrolados en el lector de huella.
2. El sistema confirma la lectura de la huella digital.
3. El sistema verifica la fecha de vencimiento del ultimo pago del socio.
4. El sistema permite el ingreso a las instalaciones.
5. El socio ingresa a las instalaciones.
6. El sistema registra el evento de ingreso, el socio, la fecha y la hora.

# Secuencias alternativas

* 2a. No se puede obtener la huela del dedo colocado en el lector
  * 2a1. El sistema muestra un mensaje de error.
* 2b. La huella obtenida no corresponde a ningún usuario conocido.
  * 2b1. El sistema muestra un mensaje de error. 
* 3a. No se registran pagos para el socio identificado
  * 3a1. El sistema muestra un mensaje de error.
  * 3b2. El sistema registra el rechazo, el socio, la fecha y la hora.
* 3b. El ultimo pago del socio se encuentra vencido.
  * 3b1. El sistema muestra un mensaje de error.
  * 3b2. El sistema registra el rechazo, el socio, la fecha y la hora.
* 5a. El socio no ingresa a las instalaciones
  * 5a1. El sistema registra la autorización sin ingreso, el socio, la fecha y la hora.

# Postcondiciones

* Se genera un registro de evento en el sistema