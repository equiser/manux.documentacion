# Manux: Documentación del producto #

Repositorio GIT que contiene la documentación del proyecto Manux: Control de socios.

### Como se utiliza el repositorio ###

En general el repositorio de documentación esta pensado para se utilizado como un submodulo del repositorio del proyecto completo. Salvo que no tenga permiso para acceder al proyecto completo usted no debería trabajar en este repositorio sino que deberia clonar este [repositorio](https://bitbucket.org/equiser/manux.git).
